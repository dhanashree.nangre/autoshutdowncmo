﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMOElastAlert
{
    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int skipped { get; set; }
        public int failed { get; set; }
    }

    public class JSONResponse
    {
        public int count { get; set; }
        public Shards _shards { get; set; }
    }
}
