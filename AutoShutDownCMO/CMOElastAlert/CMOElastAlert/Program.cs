﻿
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using RabbitMQ.Client;

namespace CMOElastAlert
{
    class Program
    {
        public static void Main(string[] args)
        {
            var client = new HttpClient();
            //List of recipes
            List<string> Recipes = new List<string> { "ais", "auric", "avianca", "belavia", "cambodia", "cathaypacific", "daallo", "edelweiss", "fastjet", "firefly", "flynas", "frenchbee", "gulfair", "hkexpress", "hopair", "indigo", "kulula", "latamglobal", "nouvelair", "omanair", "satena", "skippers", "southwest", "spicejet", "thomascook", "tuiflybe", "wingo" };
            List<string> Failing_Recipes = new List<string>();
            foreach (var recipe in Recipes)
            {
                //get number of started_scrape
                int Started_scrape = Get_response(recipe, "STARTED_SCRAPE", client).GetAwaiter().GetResult();
                //get number of Failed_scrape
                int Failed_scrape = Get_response(recipe, "FAILED_SCRAPE", client).GetAwaiter().GetResult();
                //get percentage
                var percentage = Started_scrape != 0 ? (Failed_scrape * 100) / Started_scrape : 0;
                //right now for testing pupose i have set it to 50%, we have to change it after deciding the threshhold value 
                if (percentage > 50)
                {
                    //It will send slack notification on slack
                    IntegrateWithSlackAsync(percentage, recipe).GetAwaiter();
                    Failing_Recipes.Add(recipe);
                    //To purge the queue
                }
            }
            PurgeTheQueue(Failing_Recipes);
        }
        private static async Task IntegrateWithSlackAsync(decimal Percentage, string recipe)
        {
            var webhookUrl = new Uri("https://hooks.slack.com/services/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");
            var slackClient = new SlackClient(webhookUrl);
            var message = $"*{recipe} :CMO failure is over 50%.* \n Rate is {Percentage}%";
            var response = await slackClient.SendMessageAsync(message);
        }
        public static async Task<int> Get_response(string Slug_name, String Message, HttpClient client)
        {
            //this Url is for geting the number of counts of message for specific recipe in last hour
            string url = "https://es.logging.production.tripstack.com/cmo-*/_count?source_content_type=application/json&source={%22query%22:{%22bool%22:{%22must%22:[{%22match_phrase%22:{%22slug%22:{%22query%22:%22" + Slug_name + "%22}}},{%22match_phrase%22:{%22message%22:{%22query%22:%22" + Message + "%22}}}],%22must_not%22:[],%22should%22:[],%22filter%22:{%22range%22:{%22@timestamp%22:{%22gte%22:%22now-1h%22}}}}}}";

            HttpRequestMessage requestMessage = new HttpRequestMessage();
            requestMessage.RequestUri = new Uri(url);
            requestMessage.Method = HttpMethod.Get;
            var responseMessage = await client.SendAsync(requestMessage);
            string jsonString = await responseMessage.Content.ReadAsStringAsync();
            var JSON = JsonConvert.DeserializeObject<JSONResponse>(jsonString);
            return JSON.count;
        }
        private static void PurgeTheQueue(List<string> Recipes)
        {
            //localhost connection to purge the queue
            ConnectionFactory factory = new ConnectionFactory();
            factory.HostName = "cmo-rabbitmq.staging.tripstack.com";
            factory.UserName = "admin";
            factory.Password = "jmaYsnDVHVtdLp8nzdXgLoDq";
            factory.VirtualHost = "/";
            factory.Protocol = Protocols.DefaultProtocol;
            factory.Port = AmqpTcpEndpoint.UseDefaultPort;
            //factory.Uri =new System.Uri("amqp://admin:jmaYsnDVHVtdLp8nzdXgLoDq@cmo-rabbitmq.staging.tripstack.com:5671");
            // var factory = new ConnectionFactory() { HostName = "cmo-rabbitmq.staging.tripstack.com", UserName = "dhanashree.nangre", Password = "WMGCzAJgzHXFaLNDzBUpdW3q", Port = 5672 };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueuePurge("TestQueue");
                    // to delete all the messages in queue
                    //foreach (var recipe in Recipes)
                    //{
                    //    channel.QueuePurge(recipe);
                    //}

                }
            }
        }
    }
}
